package com.company;

import java.util.List;

public interface ImageGenerator {


    List<Imageable> generate() throws NoImageException;
}
