package com.company;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class Main extends JFrame implements ActionListener {

    private ImageDao imageDao;

    private List<Imageable> imageables = new ArrayList<>();

    public List<Imageable> getImageables() {
        return imageables;
    }

    public void setImageables(List<Imageable> imageables) {
        this.imageables = imageables;
    }

    public ImageDao getImageDao() {
        return imageDao;
    }

    public void setImageDao(ImageDao imageDao) {
        this.imageDao = imageDao;
        imageDao.generateExampleData();
    }

    private JPanel jPanel = new JPanel();

    private ImageGenerator simpleImageGenerator;


    public Main() {

        createFrameLayout();
        setImageDao(new ExampleImageData());
        simpleImageGenerator = new com.company.SimpleImageGenerator(getImageDao().findAll());

        try {
            imageables = simpleImageGenerator.generate();
        } catch (NoImageException e) {
            e.printStackTrace();
        }
        for (int i = 1; i <= 4; i++) {
            JButton jButton = new JButton("");
            ImageIcon imageIcon = new ImageIcon(imageables.get(i-1).getFileName());
            jButton.setIcon(imageIcon);
            jButton.addActionListener(this);  //obsługa zdarzenia po kliknięciu przycisku a this wskazuje na to okno
            jButton.setName(imageables.get(i-1).getClass().getSimpleName());
            jPanel.add(jButton);
        }
        add(jPanel);
    }

    private void createFrameLayout() {
        setSize(500, 500); // ustawia wielkość okna na 500 na 500 pikseli
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // sprawia,że będzie działał exit
        setVisible(true); // widoczne okno
        setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
        JLabel jLabel = new JLabel("Co nie jest kotem?");
        jLabel.setFont(new Font("Serif",Font.PLAIN,24));
        jLabel.setForeground(Color.red);
        add(jLabel);
        jPanel.setLayout(new GridLayout(2, 2));
    }


    public static void main(String[] args) {
        // write your code here
        // będzie w tle tworzone
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main(); // tworzenie okna
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Kliknięto mnie");
        JButton clickedButton = (JButton) e.getSource();
        int theNumberWithTheSameClass = 0;
        for (Imageable imageable:getImageables()) {
            if (imageable.getClass().getSimpleName().equals(clickedButton.getName())) {
                theNumberWithTheSameClass++;
            }
        }
        if (theNumberWithTheSameClass == 1){
            JOptionPane.showMessageDialog(this, "Gratulacje poprawna odpowiedź");
        } else {
            JOptionPane.showMessageDialog(this, "Niepoprawna odpowiedź!");
        }
    }
}