package com.company;

import javax.swing.*;
import java.util.*;

public class SimpleImageGenerator implements ImageGenerator {

    private List<Imageable> imageDaoList = new ArrayList<>();
    // Se
    private Set<String> generatedClasses = new HashSet<>();

    public SimpleImageGenerator(List<Imageable> imageDaoList) {
        this.imageDaoList = imageDaoList;
    }

    @Override
    public List<Imageable> generate() throws NoImageException {
        Random random = new Random();
        List<Imageable> generatedImages = new ArrayList<>();
        int generatedIndex = 0;
        String className;
        while (true) {
            generatedIndex = random.nextInt(imageDaoList.size());
            className = imageDaoList.get(generatedIndex).getClass().getSimpleName();
            if (!generatedClasses.contains(className))
                break;
        }
        for (Imageable imageable : imageDaoList) {
            if (imageable.getClass().getSimpleName().equals(className)) {
                generatedImages.add(imageable);
            }
        } // dorzuca wszystkie element które są takie same jak wyloswany element

        // dorzucić jeden element, który nie pasuje do układanki
        while (true) {
            generatedIndex = random.nextInt(imageDaoList.size());
            if (!imageDaoList.get(generatedIndex).getClass().getSimpleName().equals(className)) {
                generatedImages.add(imageDaoList.get(generatedIndex));
                break;
            }
        }
        generatedClasses.add(className);

        return generatedImages;
    }


}